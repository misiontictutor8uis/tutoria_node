const {Schema, model} = require('mongoose');

const usuario = {nombre: String, telefono: Number, contrasena: String};
const usuarioEsquema = Schema( usuario );

module.exports = model("usuario",usuarioEsquema);